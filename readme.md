From https://superuser.com/questions/1420328/streaming-rtsp-with-ffmpeg


# Working from file
docker run --rm -it -v $PWD/rtsp-simple-server.yml:/rtsp-simple-server.yml -p 8554:8554 aler9/rtsp-simple-server
ffmpeg -re -stream_loop -1 -i test.mp4 -f rtsp -rtsp_transport tcp rtsp://localhost:8554/live
ffplay -rtsp_transport tcp rtsp://localhost:8554/live


# Working from cam
docker run --rm -it -v $PWD/rtsp-simple-server.yml:/rtsp-simple-server.yml -p 8554:8554 aler9/rtsp-simple-server
sh pipeline_from_cam.sh
ffplay -rtsp_transport tcp rtsp://localhost:8554/cam




# Original post

FWIW, I was able to setup a local RTSP server for testing purposes using simple-rtsp-server and ffmpeg following these steps:

Create a configuration file for the RTSP server called rtsp-simple-server.yml with this single line:
protocols: [tcp]
Start the RTSP server as a Docker container:
    $ docker run --rm -it -v $PWD/rtsp-simple-server.yml:/rtsp-simple-server.yml -p 8554:8554 aler9/rtsp-simple-server
Use ffmpeg to stream a video file (looping forever) to the server:
    $ ffmpeg -re -stream_loop -1 -i test.mp4 -f rtsp -rtsp_transport tcp rtsp://localhost:8554/live
Once you have that running you can use ffplay to view the stream:

    $ ffplay -rtsp_transport tcp rtsp://localhost:8554/live
Note that simple-rtsp-server can also handle UDP streams (i.s.o. TCP) but that's tricky running the server as a Docker container.





ffmpeg -re  -i /dev/video0 -f rtsp -rtsp_transport tcp rtsp://localhost:8554/cam
ffplay -rtsp_transport tcp rtsp://localhost:8554/cam








# Quite fast and detailed
ffmpeg -re -i /dev/video0 -pix_fmt yuv420p -vsync 1 -threads 0 -vcodec libx264 -r 30 -g 60 -sc_threshold 0 -b:v 512k -bufsize 640k -maxrate 640k -preset veryfast -profile:v baseline -tune film -acodec aac -b:a 128k -ac 2 -ar 48000 -af "aresample=async=1:min_hard_comp=0.100000:first_pts=0" -bsf:v h264_mp4toannexb -f rtsp -rtsp_transport tcp rtsp://localhost:8554/cam
