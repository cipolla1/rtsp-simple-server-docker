# Fast and detailed camera stream pipeline using ffmpeg

ffmpeg -re -i /dev/video0 \
    -pix_fmt yuv420p \
    -vsync 1 -threads 0 -vcodec libx264 -r 30 -g 60 -sc_threshold 0 \
    -b:v 512k -bufsize 640k -maxrate 640k \
    -preset veryfast -profile:v baseline \
    -tune film -acodec aac \
    -f rtsp -rtsp_transport tcp rtsp://localhost:8554/cam


# Source of the pipeline
#https://www.wowza.com/docs/how-to-restream-using-ffmpeg-with-wowza-streaming-engine#:~:text=To%20re%2Dstream%20using%20FFmpeg,at%20its%20native%20frame%20rate.